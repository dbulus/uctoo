<?php
// +----------------------------------------------------------------------
// | CatchAdmin [Just Like ～ ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017~2021 http://catchadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( https://github.com/yanwenwu/catch-admin/blob/master/LICENSE.txt )
// +----------------------------------------------------------------------
// | Author: JaguarJack [ njphper@gmail.com ]
// +----------------------------------------------------------------------

// wechatopen路由
$router->group(function () use ($router){
    // authEvent路由
    $router->post('wechatopen/authevent', '\app\uctoo\wechatopen\controller\Wechatopen@authEvent');
    // EventMessage路由
    $router->post('wechatopen/eventmessage/appid/<appid>', '\app\uctoo\wechatopen\controller\Wechatopen@EventMessage');
    //获取扫码授权URL接口
    $router->get('component/authorize', '\app\uctoo\wechatopen\controller\Component@authorize');
    //扫码授权后处理数据
    $router->post('component/getauthorization/[:auth_code]', '\app\uctoo\wechatopen\controller\Component@getAuthorization')->allowCrossDomain();
    // test路由
    $router->post('wechatopen/test', '\app\uctoo\wechatopen\controller\Test@index');
    // wechat路由  微信小商店 标准版交易组件
    $router->post('api/wechatopen/product/category/get', '\app\uctoo\wechatopen\api\Wechatopen@productCategoryGet');
    $router->post('api/wechatopen/product/brand/get', '\app\uctoo\wechatopen\api\Wechatopen@productBrandGet');
    $router->post('api/wechatopen/product/delivery/get_freight_template', '\app\uctoo\wechatopen\api\Wechatopen@productDeliveryGet_freight_template');
    $router->post('api/wechatopen/product/store/get_shopcat', '\app\uctoo\wechatopen\api\Wechatopen@productStoreGet_shopcat');
    $router->post('api/wechatopen/product/spu/add', '\app\uctoo\wechatopen\api\Wechatopen@productSpuAdd');
    $router->post('api/wechatopen/product/spu/del', '\app\uctoo\wechatopen\api\Wechatopen@productSpuDel');
    $router->post('api/wechatopen/product/spu/get', '\app\uctoo\wechatopen\api\Wechatopen@productSpuGet');
    $router->post('api/wechatopen/product/spu/get_list', '\app\uctoo\wechatopen\api\Wechatopen@productSpuGet_list');
    $router->post('api/wechatopen/product/spu/search', '\app\uctoo\wechatopen\api\Wechatopen@productSpuSearch');
    $router->post('api/wechatopen/product/spu/update', '\app\uctoo\wechatopen\api\Wechatopen@productSpuUpdate');
    $router->post('api/wechatopen/product/spu/listing', '\app\uctoo\wechatopen\api\Wechatopen@productSpuListing');
    $router->post('api/wechatopen/product/spu/delisting', '\app\uctoo\wechatopen\api\Wechatopen@productSpuDelisting');
    $router->post('api/wechatopen/product/sku/add', '\app\uctoo\wechatopen\api\Wechatopen@productSkuAdd');
    $router->post('api/wechatopen/product/sku/del', '\app\uctoo\wechatopen\api\Wechatopen@productSkuDel');
    $router->post('api/wechatopen/product/sku/get', '\app\uctoo\wechatopen\api\Wechatopen@productSkuGet');
    $router->post('api/wechatopen/product/sku/get_list', '\app\uctoo\wechatopen\api\Wechatopen@productSkuGet_list');
    $router->post('api/wechatopen/product/sku/update', '\app\uctoo\wechatopen\api\Wechatopen@productSkuUpdate');
    $router->post('api/wechatopen/product/sku/update_price', '\app\uctoo\wechatopen\api\Wechatopen@productSkuUpdate_price');
    $router->post('api/wechatopen/product/stock/update', '\app\uctoo\wechatopen\api\Wechatopen@productStockUpdate');
    $router->post('api/wechatopen/product/stock/get', '\app\uctoo\wechatopen\api\Wechatopen@productStockGet');
    $router->post('api/wechatopen/product/order/get_list', '\app\uctoo\wechatopen\api\Wechatopen@productOrderGet_list');
    $router->post('api/wechatopen/product/order/get', '\app\uctoo\wechatopen\api\Wechatopen@productOrderGet');
    $router->post('api/wechatopen/product/order/search', '\app\uctoo\wechatopen\api\Wechatopen@productOrderSearch');
    $router->post('api/wechatopen/product/delivery/get_company_list', '\app\uctoo\wechatopen\api\Wechatopen@productDeliveryGet_company_list');
    $router->post('api/wechatopen/product/delivery/send', '\app\uctoo\wechatopen\api\Wechatopen@productDeliverySend');
    $router->post('api/wechatopen/product/store/get_info', '\app\uctoo\wechatopen\api\Wechatopen@productStoreGet_info');
    $router->post('api/wechatopen/product/img/upload', '\app\uctoo\wechatopen\api\Wechatopen@productImgUpload');
    // 第三方平台小程序
    $router->post('api/wechatopen/sns/component/jscode2session', '\app\uctoo\wechatopen\api\Wechatopen@snsComponentJscode2session');
    // 无限量小程序码 https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/qr-code/wxacode.getUnlimited.html
    $router->post('api/wechatopen/wxa/getwxacodeunlimit', '\app\uctoo\wechatopen\api\Wechatopen@wxaGetwxacodeunlimit');
    //查询小程序插件使用列表
    $router->post('api/wechatopen/wxa/plugin/list', '\app\uctoo\wechatopen\api\Wechatopen@wxaPluginList');
    //申请小程序插件使用
    $router->post('api/wechatopen/wxa/plugin/apply', '\app\uctoo\wechatopen\api\Wechatopen@wxaPluginApply');
    //获取小程序用户手机号码
    $router->post('api/wechatopen/wxa/business/getuserphonenumber', '\app\uctoo\wechatopen\api\Wechatopen@wxaGetUserPhoneNumber');
    //小程序私密消息
    $router->post('api/wechatopen/message/wxopen/activityid/create', '\app\uctoo\wechatopen\api\Wechatopen@messageWxopenActivityidCreate');
});