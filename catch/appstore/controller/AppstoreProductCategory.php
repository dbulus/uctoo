<?php

namespace catchAdmin\appstore\controller;

use app\BaseController;
use catcher\base\CatchRequest as Request;
use catcher\CatchAdmin;
use catcher\CatchResponse;
use catcher\base\CatchController;
use catchAdmin\minishop\model\MinishopProductCategory as MinishopProductCategoryModel;
use catcher\exceptions\FailedException;
use catcher\Utils;
use think\App;
use uctoo\uctoocloud\client\Http;
use catchAdmin\wechatopen\model\AdminApplet;

class AppstoreProductCategory extends CatchController
{
    protected $MinishopProductCategoryModel;

    protected function initialize()
    {
        $this->MinishopProductCategoryModel = new MinishopProductCategoryModel;
    }
    /**
     * 列表
     * @time 2021年06月10日 20:29
     * @param Request $request 
     */
    public function index(Request $request)
    {
        $result = null;
        trace('Requestindex','debug');
        $host = trim(Utils::config('appstoreserver.host'));//获取管理后台配置的微信第三方平台地址 http://serv.uctoo.com
        $data = input('post.');
        $response = Http:: withHost($host)   //指定第三方平台
        ->withVerify(false)    //无需SSL验证
        ->product('')    //如果指定产品 则必填参数
        ->get('/appstoreserver/product/category', $data);  //当前输入的应用市场帐号
        trace($response,'debug');
        $res = json_decode($response->body(),true);
        trace($res,'debug');
        if($res['code'] == 10000){

        }
        return json($res);
    }
}