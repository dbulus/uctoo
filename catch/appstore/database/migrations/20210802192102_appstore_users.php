<?php

use think\migration\Migrator;
use think\migration\db\Column;
use Phinx\Db\Adapter\MysqlAdapter;

class AppstoreUsers extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('appstore_users', ['engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '应用市场用户' ,'id' => 'id' ,'primary_key' => ['id']]);
        $table->addColumn('email', 'string', ['limit' => 100,'null' => false,'default' => '','signed' => true,'comment' => '邮箱 登录',])
			->addColumn('avatar', 'string', ['limit' => 255,'null' => false,'default' => '','signed' => true,'comment' => '用户头像',])
			->addColumn('remember_token', 'string', ['limit' => 512,'null' => false,'default' => '','signed' => true,'comment' => '用户token',])
			->addColumn('appid', 'string', ['limit' => 100,'null' => true,'signed' => true,'comment' => 'appid',])
			->addColumn('env', 'string', ['limit' => 255,'null' => true,'signed' => true,'comment' => '云环境ID',])
			->addColumn('created_at', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => false,'comment' => '创建时间',])
			->addColumn('updated_at', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => false,'comment' => '更新时间',])
			->addColumn('deleted_at', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => false,'comment' => '软删除字段',])
			->addColumn('creator_id', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => false,'comment' => '创建人ID',])
            ->create();
    }
}
