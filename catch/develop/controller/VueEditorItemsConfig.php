<?php
// +----------------------------------------------------------------------
// | UCToo [ Universal Convergence Technology ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014-2022 https://www.uctoo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: UCToo <contact@uctoo.com>
// +----------------------------------------------------------------------
namespace catchAdmin\develop\controller;

use catcher\base\CatchRequest as Request;
use catcher\CatchResponse;
use catcher\base\CatchController;
use catchAdmin\develop\model\VueEditorItemsConfig as VueEditorItemsConfigModel;
use think\facade\Log;
use think\Response;
use catchAdmin\develop\request\VueEditorItemsConfigRequest;
use think\facade\Db;

class VueEditorItemsConfig extends CatchController
{
    
    protected $vueEditorItemsConfigModel;
    
    /**
     *
     * @time 2022/03/13 18:21
     * @param VueEditorItemsConfigModel $vueEditorItemsConfigModel
     * @return mixed
     */
    public function __construct(VueEditorItemsConfigModel $vueEditorItemsConfigModel)
    {
        $this->vueEditorItemsConfigModel = $vueEditorItemsConfigModel;
    }
    
    /**
     *
     * @time 2022/03/13 18:21
     * @return Response
     */
    public function index() : Response
    {
        return CatchResponse::paginate($this->vueEditorItemsConfigModel->getList());
    }
    
    /**
     *
     * @time 2022/03/13 18:21
     * @param Request $request
     * @return Response
     */
    public function save(Request $request) : Response
    {
        return CatchResponse::success($this->vueEditorItemsConfigModel->storeBy($request->post()));
    }
    
    /**
     *
     * @time 2022/03/13 18:21
     * @param $id
     * @return Response
     */
    public function read($id) : Response
    {
        return CatchResponse::success($this->vueEditorItemsConfigModel->findBy($id));
    }
    
    /**
     *
     * @time 2022/03/13 18:21
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function update($id, Request $request) : Response
    {
        return CatchResponse::success($this->vueEditorItemsConfigModel->updateBy($id, $request->post()));
    }
    
    /**
     *
     * @time 2022/03/13 18:21
     * @param $id
     * @return Response
     */
    public function delete($id) : Response
    {
        return CatchResponse::success($this->vueEditorItemsConfigModel->deleteBy($id));
    }

    /**
     *
     * @time 2022/03/13 18:21
     * @param Request $request
     * @return Response
     */
    public function saveAll(VueEditorItemsConfigRequest $request) : Response
    {
       //
        $data = $request->post('saveData');
        $page_id = input('page_id');
        $data1 = json_decode('[{"name":"Text","value":{"id":11,"txt":"抢购商品","txtColor":"#507aa0"}},{"name":"FlashSaleGoodsList","value":{"id":12,"startTime":"2022-03-14T07:23:57.587Z","seckillBrand":{"imgUrl":"https://img30.360buyimg.com/babel/s290x370_jfs/t1/133470/23/5172/252715/5f1a3b3cE2c82f4cf/4a92d18397680eee.jpg!cc_290x370.webp","imgLink":"https://www.jd.com"},"goodsList":[{"imgUrl":"https://img20.360buyimg.com/seckillcms/s140x140_jfs/t1/126328/18/7901/172630/5f1a59abE28e964aa/f5e20e27f28b0a89.jpg.webp","imgLink":"https://www.jd.com"},{"imgUrl":"https://img30.360buyimg.com/seckillcms/s140x140_jfs/t1/142185/40/3434/109046/5f16ac4fE956bf200/90efad9b84ffe90b.jpg.webp","imgLink":"https://www.jd.com"},{"imgUrl":"https://img14.360buyimg.com/seckillcms/s140x140_jfs/t1/133076/17/1286/104358/5ed75fc3E23ef61d0/b978578df3ac20be.jpg.webp","imgLink":"https://www.jd.com"},{"imgUrl":"https://img14.360buyimg.com/seckillcms/s140x140_jfs/t1/118303/36/12735/201862/5f190d0eEdc4b3f3e/aa56786b0830a522.jpg.webp","imgLink":"https://www.jd.com"},{"imgUrl":"https://img14.360buyimg.com/seckillcms/s140x140_jfs/t1/124035/6/7608/109674/5f1535f4E446ef9a3/5d16b10d7fdd226e.jpg.webp","imgLink":"https://www.jd.com"}]}},{"name":"Text","value":{"id":13,"txt":"超值进口新发现","txtColor":"#ff0132"}},{"name":"MultipleImg5","value":{"imgList":[{"imgUrl":"https://img.alicdn.com/tps/i4/TB1Q2Mnd2zO3e4jSZFxwu1P_FXa.png_500x1000q75.jpg_.webp","imgLink":"https://www.tmall.com"},{"imgUrl":"https://img.alicdn.com/tps/i4/TB1t2dzOvb2gK0jSZK9wu1EgFXa.png_500x1000q75.jpg_.webp","imgLink":"https://www.tmall.com"},{"imgUrl":"https://img.alicdn.com/tps/i4/TB1ZJtFOAL0gK0jSZFAwu3A9pXa.png_500x1000q75.jpg_.webp","imgLink":"https://www.tmall.com"},{"imgUrl":"https://img.alicdn.com/tps/i4/TB1y4tuOxz1gK0jSZSgSuuvwpXa.jpg_500x1000q75s0.jpg_.webp","imgLink":"https://www.tmall.com"},{"imgUrl":"https://img.alicdn.com/tps/i4/TB14GVJOAT2gK0jSZFkwu3IQFXa.png_500x1000q75.jpg_.webp","imgLink":"https://www.tmall.com"}]}},{"name":"Text","value":{"txt":"超值进口新发现","txtColor":"#487cff"}},{"name":"MultipleImg2_3","value":{"id":16,"imgItem1_1":{"imgUrl":"https://img.alicdn.com/tps/i4/TB1fbhiawoQMeJjy0FnSuv8gFXa.jpg_490x490q100.jpg_.webp","imgLink":"https://www.jd.com/"},"imgItem1_2":{"imgUrl":"https://gw.alicdn.com/tfs/TB1UzOqoWL7gK0jSZFBXXXZZpXa-468-602.jpg","imgLink":"https://www.jd.com/"},"imgItem2_1":{"imgUrl":"https://img.alicdn.com/tfs/TB1XjMYnfb2gK0jSZK9XXaEgFXa-468-1236.jpg","imgLink":"https://www.jd.com/"},"imgItem2_2":{"imgUrl":"https://gw.alicdn.com/tfs/TB1s720BxD1gK0jSZFyXXciOVXa-468-1236.jpg","imgLink":"https://www.jd.com/"},"imgItem2_3":{"imgUrl":"https://gw.alicdn.com/tfs/TB1xVR9oFP7gK0jSZFjXXc5aXXa-468-602.jpg","imgLink":"https://www.jd.com/"}}},{"name":"Text","value":{"id":17,"txt":"超值进口新发现","txtColor":"#589A6D"}},{"name":"MultipleImg1_3","value":{"id":18,"imgItem1_1":{"imgUrl":"https://aecpm.alicdn.com/simba/img/TB1W4nPJFXXXXbSXpXXSutbFXXX.jpg","imgLink":"http://127.0.0.1:8800/vue-editor.html#/editor"},"imgItem2_1":{"imgUrl":"https://img.alicdn.com/tps/i4/TB1KhvGOAY2gK0jSZFgSuw5OFXa.jpg","imgLink":"http://127.0.0.1:8800/vue-editor.html#/editor"},"imgItem2_2":{"imgUrl":"https://img.alicdn.com/tps/i4/TB17qddaOERMeJjSspiSuvZLFXa.jpg_320x5000q100.jpg_.webp","imgLink":"http://127.0.0.1:8800/vue-editor.html#/editor"},"imgItem2_3":{"imgUrl":"https://img.alicdn.com/tfs/TB1XjMYnfb2gK0jSZK9XXaEgFXa-468-1236.jpg","imgLink":"http://127.0.0.1:8800/vue-editor.html#/editor"}}},{"name":"CategoryGoods","value":{"id":19,"title":"打造爱巢","subTitle":"HOME","banner":{"link":{"imgUrl":"https://img.alicdn.com/tps/i4/TB1MesKcWmWQ1JjSZPhwu0CJFXa.png","imgLink":"https://www.jd.com"},"bannerTitle":"广告位","bannerSubTitle":"广告位副标题"},"goodsList":[{"imgUrl":"https://gw.alicdn.com/bao/uploaded/i3/3243519086/O1CN016gQ0Ia2GzR82IAhdR_!!0-item_pic.jpg","imgLink":"https://www.jd.com"},{"imgUrl":"https://gw.alicdn.com/bao/uploaded/i1/2145455856/O1CN01tJoDJ11t85wjbqfVw_!!0-item_pic.jpg","imgLink":"https://www.jd.com"},{"imgUrl":"https://gw.alicdn.com/bao/uploaded/i1/2750439771/TB14Nr2MVXXXXbMapXXXXXXXXXX_!!0-item_pic.jpg","imgLink":"https://www.jd.com"},{"imgUrl":"https://gw.alicdn.com/bao/uploaded/i2/2619099633/O1CN01XQNyC82L1xuQm6aeX_!!0-item_pic.jpg","imgLink":"https://www.jd.com"},{"imgUrl":"https://gw.alicdn.com/bao/uploaded/i1/748159429/O1CN01xeiYck2JWX37gKDJk_!!2-item_pic.png","imgLink":"https://www.jd.com"},{"imgUrl":"https://gw.alicdn.com/bao/uploaded/i1/3294603858/O1CN01HpuPy31eN0YdGTTkz_!!2-item_pic.png","imgLink":"https://www.jd.com"},{"imgUrl":"https://gw.alicdn.com/bao/uploaded/i1/2105113961/O1CN01C6b1kj1f8BJxju0i4_!!0-item_pic.jpg","imgLink":"https://www.jd.com"},{"imgUrl":"https://gw.alicdn.com/bao/uploaded/i3/670743919/TB1ikrMawnH8KJjSspcXXb3QFXa_!!0-item_pic.jpg","imgLink":"https://www.jd.com"}]}},{"name":"RecommendedGoodsList","value":{"id":20}},{"name":"AllGoodsList","value":{"id":21}}]',true);
        $oldIDs = $this->vueEditorItemsConfigModel->where('page_id', $page_id)->column('id');
        $newIDs = array_column( $data , 'id' );
        $diffIDs = array_diff($oldIDs,$newIDs);


        $delRes = Db::name('vue_editor_items_config')     //未知原因模型中的delete方法无效，就用Db了。对应前端删除操作
            ->where('page_id', $page_id)
           // ->useSoftDelete('deleted_at',time())
            ->delete();
        //$delRes = $this->vueEditorItemsConfigModel->where('page_id','=',1)->delete();
        $saveData = [];
        foreach ($data as $k => $v){
            unset($v['$id']);           //去掉前端增加了一个多余字段
         //   if(!isset($v['value']['id'])){   //如果value中不包含id字段，则是前端新增的控件，去掉前端自动增加的id,saveAll时后端生成自增id
                unset($v['id']);
         //   }
            $v['value'] = json_encode($v['value'],JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE );//value字段保存json数据
            $saveData[$k] = $v;
        }
        $list = [
            0 => ['name'=>'Text','value'=>'{
		"id": 11,
		"txt": "抢购",
		"txtColor": "#507aa0"
	}'],1 => ['name'=>'Text','value'=>'']
        ];
        //Log::write($data, 'debug');
        $res = $this->vueEditorItemsConfigModel->allowField(['id','type','category','name','value','page_id','schema','uiSchema','formData','errorSchema','formFooter','formProps','deleted_at'])->saveAll($saveData);
        $allData = $this->vueEditorItemsConfigModel->where('deleted_at','=',0)->select();
        return CatchResponse::success($res);
    }
}