<?php
// +----------------------------------------------------------------------
// | UCToo [ Universal Convergence Technology ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014-2022 https://www.uctoo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: UCToo <contact@uctoo.com>
// +----------------------------------------------------------------------
use think\migration\Migrator;
use think\migration\db\Column;
use Phinx\Db\Adapter\MysqlAdapter;

class VueEditorItemsConfig extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('vue_editor_items_config', ['engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '页面配置' ,'id' => 'id' ,'primary_key' => ['id']]);
        $table->addColumn('type', 'string', ['limit' => 128,'null' => true,'signed' => true,'comment' => '类型',])
			->addColumn('category', 'string', ['limit' => 128,'null' => true,'signed' => true,'comment' => '分类',])
			->addColumn('name', 'string', ['limit' => 128,'null' => false,'default' => '','signed' => true,'comment' => '组件名称',])
			->addColumn('value', 'json', ['null' => false,'signed' => true,'comment' => '组件配置json',])
			->addColumn('page_id', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => true,'comment' => '归属页面ID',])
			->addColumn('schema', 'json', ['null' => true,'signed' => true,'comment' => 'schema',])
			->addColumn('uiSchema', 'json', ['null' => true,'signed' => true,'comment' => 'uiSchema',])
			->addColumn('formData', 'json', ['null' => true,'signed' => true,'comment' => 'formData',])
			->addColumn('errorSchema', 'json', ['null' => true,'signed' => true,'comment' => 'errorSchema',])
			->addColumn('formFooter', 'json', ['null' => true,'signed' => true,'comment' => 'formFooter',])
			->addColumn('formProps', 'json', ['null' => true,'signed' => true,'comment' => 'formProps',])
			->addColumn('created_at', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => false,'comment' => '创建时间',])
			->addColumn('updated_at', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => false,'comment' => '更新时间',])
			->addColumn('deleted_at', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => false,'comment' => '软删除字段',])
			->addColumn('creator_id', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => false,'comment' => '创建人ID',])
            ->create();
    }
}
