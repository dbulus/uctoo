codelabs模块可以用来可视化配置和生成vue、vue-element等页面文件源码。  

# 概述  

  codelabs目前有SaaS免费版和独立部署版，两个版本。SaaS免费版可在www.uctoo.com注册帐号使用。独立部署版，可在UCToo独立部署环境安装develop模块使用。由于codelabs模块属于开发阶段的工具，有很多可能更改所部署环境项目代码的功能，因此请慎重将develop模块提供给除系统管理员、开发者以外的用户角色使用。codelabs SaaS
  免费版仅提供了一个功能子集。独立部署版包含所有的功能全集。本文档主要介绍独立部署版的使用。
  codelabs主要由模板区、数据结构区、算法区、模板配置区、代码生成区5个部分组成。每个功能区完成低代码/无代码可视化配置的一部分工作。完成每个功能区的配置后，都应进行保存，以作为代码生成的前置配置。
  
## 使用手册
  模块使用界面截图：
  <table>
      <tr>
          <td><img src="https://gitee.com/UCT_admin/materials/raw/master/uctoo/lowcode/codelabs.png"></td>
      </tr>
      <tr>
          <td><img src="https://gitee.com/UCT_admin/materials/raw/master/uctoo/lowcode/elmform.png"></td>
      </tr>
      <tr>
          <td><img src="https://gitee.com/UCT_admin/materials/raw/master/uctoo/lowcode/component_config.png"></td>
      </tr>
      <tr>
          <td><img src="https://gitee.com/UCT_admin/materials/raw/master/uctoo/lowcode/jsparser.png"></td>
      </tr>
      <tr>
          <td><img src="https://gitee.com/UCT_admin/materials/raw/master/uctoo/lowcode/jsmerge.png"></td>
      </tr>
  </table>
  
### 模版
  <table>
      <tr>
          <td><img src="https://gitee.com/UCT_admin/materials/raw/master/uctoo/lowcode/codelabs_template.png"></td>
      </tr>
  </table>
1. 模板区主要由模板代码和模板文件两个tab组成。开发者可使用模板代码区录入模板源码进行开发测试(可复制elmform.stub的全部内容到模板代码区测试)。可在模板文件区选择已发布的模板或控件用于代码生成。  
2. 如果输入了模版代码则优先使用输入的模版代码进行代码生成。如果输入模版代码为空，则采用选择的模版文件进行代码生成。  
3. elmform.stub模板用于生成headless CMS模块默认CURD界面，加入了版本管理，v1.0.0采用gitee企业版管理后台界面风格。  

### 数据结构
  <table>
      <tr>
          <td><img src="https://gitee.com/UCT_admin/materials/raw/master/uctoo/lowcode/codelabs_datastructure.png"></td>
      </tr>
  </table>
1. 数据结构区主要由数据结构和数据源两个tab组成。开发者可在数据结构区录入用于描述模板控件结构或者用于渲染模板的数据结构，一般低代码/无代码产品都采用json数据结构以及json解析器的方案，UCToo同样沿用了这一技术路线。目前并未严格定义json数据结构，开发者可以自由发挥。  
2. 如果输入了数据结构则优先使用输入的数据结构进行代码生成（免费版必须人工输入数据结构）。如果输入数据结构为空，则采用数据源的配置进行代码生成。免费版codelabs不支持数据源自动获取数据结构功能（仍然需要配置数据源）。独立部署版可使用数据源自动获取数据结构功能，配置从本地数据库表获取数据结构。  
3. 目前数据源配置，仅有表名字段是真正定义数据源的。模块、模型、控制器本质上是属于模板配置区的属性，后续版本将优化此功能区。目前已支持通过配置的表名的表结构初始化elmform.stub模板的配置区展示界面。  
4. 如下数据结构是用于生成CURD页面的一个典型数据结构，可以通过修改以下数据结构的值，进行elmform.stub模板配置的初始化和生成最终代码。  

```json
{
	"controller": {
		"module": "test",
		"controller": "catchAdmin\\test\\controller\\test",
		"table": "test",
		"model": "catchAdmin\\test\\model\\test",
		"restful": true
	},
	"table_fields": [{
		"field": "module",
		"type": "varchar",
		"length": 50,
		"nullable": false,
		"index": "",
		"default": "",
		"comment": "模块",
		"unsigned": false,
		"inputType": "input"
	}, {
		"field": "operate",
		"type": "varchar",
		"length": 20,
		"nullable": false,
		"index": "",
		"default": "",
		"comment": "操作",
		"unsigned": false,
		"inputType": "textarea"
	}, {
		"field": "route",
		"type": "varchar",
		"length": 100,
		"nullable": false,
		"index": "",
		"default": "",
		"comment": "路由",
		"unsigned": false,
		"inputType": "input"
	}],
	"table_extra": {
		"primary_key": "id",
		"created_at": true,
		"soft_delete": true,
		"creator_id": true,
		"engine": "InnoDB",
		"comment": "测试"
	},
	"create_controller": true,
	"create_model": true,
	"create_migration": true,
	"create_table": true,
	"create_crudform": true
}
```

### 算法
<table>
      <tr>
          <td><img src="https://gitee.com/UCT_admin/materials/raw/master/uctoo/lowcode/codelabs_algorithm.png"></td>
      </tr>
  </table>
1. 暂不支持算法代码的在线运行。目前只支持通过选择的算法文件生成代码。create:elmform是用于从elmform.stub模板结合用户可视化配置的输入信息，生成管理后台CURD页面的命令行工具。

### 模板配置
  <table>
      <tr>
          <td><img src="https://gitee.com/UCT_admin/materials/raw/master/uctoo/lowcode/codelabs_templateconfig.png"></td>
      </tr>
  </table>
1. 保存了模板和数据结构后，就可以通过模板配置->初始配置获取模板初始配置界面。可以在模板配置区域进行可视化界面配置。  
2. 选择了字段对应的控件类型后，可以通过操作->配置按钮，对控件进行可视化配置。对模板及控件的配置，基本都是填空和选择操作。具体控件和模板的详细参数配置说明，后续会提供一个分类检索和展示机制。保存控件配置后，即已生成控件源码并保存在字段对应的component_code节点。  
3. 保存模板配置的数据用于代码生成。  

### 代码生成
1. 页面文件的正常运行和文件路径、文件名等数据相关，因此代码生成前需要配置输出设置。（免费版不支持生成代码保存至文件，但仍需设置）  
2. 以上配置完成后，通过代码生成功能，即可获得生成的代码。生成的代码保存至输出设置对应的目录和文件名即可在前端框架中正常使用。（对应的后端接口需能正常访问）  

### 登录应用市场
1. 通过登录应用市场功能。可以使用www.uctoo.com的帐号，获取到登录状态，已登录状态的codelabs模块，模板文件功能的选项数据和算法文件功能的选项数据，将从www.uctoo.com 运营平台获取。后续将可支持不开源分发的模板、算法的代码生成。  
2. Codelabs模块也支持从本地数据库获取模板文件和算法文件数据。在系统管理->配置管理->SaaS配置，将站点类型配置为运营平台，即可不用登录UCToo应用市场，全部从本地数据库获取Codelabs的所有数据。  

## 与catchadmin的代码生成器结合使用  

1. catchadmin代码生成器可以可视化的定义后台模块、表结构，生成控制器、模型、表迁移等后台脚手架。UCToo扩展增加了创建菜单和保存生成记录的功能。  
2. 保存生成记录，可以将后台代码生成的数据保存到codelabs中，可用于初始化codelabs工具。也有利于记录应用开发、配置的历史数据，形成可回溯的开发工作数据沉淀。  
3. 通过develop-> codelabsCURD页面，可以查看所有代码生成的结果，可以通过操作按钮使用一条codelabs数据初始化打开codelabs模块进行前端代码生成。形成一条完整的前后端低代码开发工具链。  
4. 生成的管理后台数据curd页面，即可以通过生成的菜单直接访问和使用。通过菜单管理可以自定义修改默认生成的菜单名称。  