<?php
// +----------------------------------------------------------------------
// | UCToo [ Universal Convergence Technology ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014-2022 https://www.uctoo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: UCToo <contact@uctoo.com>
// +----------------------------------------------------------------------
namespace catchAdmin\develop\model;

use catcher\base\CatchModel as Model;
use catchAdmin\develop\model\VueEditorItemsConfig;
/**
 *
 * @property int $id
 * @property string $name
 * @property string $type
 * @property string $image
 * @property string $memo
 * @property string $platform
 * @property string $page_title
 * @property string $page_path
 * @property int $user_id
 * @property int $appstore_user_id
 * @property int $status
 * @property int $edit_status
 * @property string $tags
 * @property string $description
 * @property int $created_at
 * @property int $updated_at
 * @property int $deleted_at
 * @property int $creator_id
 */
class VueEditorPages extends Model
{
    
    public $field = [
        //
        'id',
        // 页面名称
        'name',
        // 页面类型
        'type',
        // 页面预览图
        'image',
        // 备注
        'memo',
        // 适用平台
        'platform',
        // 页面标题
        'page_title',
        // 页面路径
        'page_path',
        // 关联users表id
        'user_id',
        // 关联appstore用户id
        'appstore_user_id',
        // 状态
        'status',
        // 编辑状态
        'edit_status',
        // 分类标签
        'tags',
        // 页面说明
        'description',
        // 创建时间
        'created_at',
        // 更新时间
        'updated_at',
        // 软删除字段
        'deleted_at',
        // 创建人ID
        'creator_id',
    ];
    
    public $name = 'vue_editor_pages';

    public function itemsConfig()
    {
        return $this->hasMany(VueEditorItemsConfig::class,'page_id');
    }

    /**
     * 查询列表
     *
     * @time 2020年04月28日
     * @return mixed
     */
    public function getList()
    {
        // 不分页
        if (property_exists($this, 'paginate') && $this->paginate === false) {
            return $this->catchSearch()->with(['itemsConfig'])
                ->field('*')
                ->catchOrder()
                ->creator()
                ->select();
        }

        // 分页列表
        return $this->catchSearch()->with(['itemsConfig'])
            ->field('*')
            ->catchOrder()
            ->creator()
            ->paginate();
    }
}