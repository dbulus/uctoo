<?php
// +----------------------------------------------------------------------
// | UCToo [ Universal Convergence Technology ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014-2022 https://www.uctoo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: UCToo <contact@uctoo.com>
// +----------------------------------------------------------------------
namespace catchAdmin\develop\request;

use catcher\base\CatchRequest;

class VueEditorItemsConfigRequest extends CatchRequest
{
    protected $needCreatorId = false;  //saveAll方法不能带CreatorId,会破坏队形

    protected function rules(): array
    {
        return [];
    }

    protected function message(): array
    {
        // TODO: Implement message() method.
        return [];

    }
}
