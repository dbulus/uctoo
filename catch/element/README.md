element 模块是一个可视化配置vue element模板/控件，生成源码的模块，与codelabs模块配合使用，作为element的模板/控件库。  

# 概述  

  本模块的设计目标是提供开发人员、产品人员等相关角色，可以可视化的配置和生成vue element源码。  
  
## 主要特性  

### 一. element components 模块  
1. 支持通过可复用的模板，可视化配置和生成vue、vue-element等页面文件源码，开发者只需掌握vue知识，此外无任何额外学习成本。  
2. view目录保存控件模板。
3. ElementComponents类中实现控件配置初始化方法和控件代码生成方法（有点类似服务端渲染 SSR）。
4. 极其简单的可视化配置控件扩展机制。

## 产品架构  
### 原理
  codelabs模块实现了一个典型的低代码、无代码运行逻辑框架，最好不在codelabs模块与特定前端框架强耦合。element模块用于codelabs模块与特定前端框架解耦，element模块保存和实现element ui模板和控件的初始化和服务端代码生成。

1. 一般低代码/无代码框架，都是将上游技术栈的可配置项进行一个可视化封装提供给用户使用。但是上游技术栈可配置项的排列组合非常多，全量提供给用户是不合适的。
  一般项目都是固定下一种排列组合，形成有特定风格的一套界面模板，例如UCToo代码生成的界面是类似gitee管理后台风格。view目录下的控件模板采用了TP模板的机制，可配置变量形如 {$tables}。  
2. element组件的全量配置项保存在element_component表specification字段（暂时未用到）。
3. 模板的可配置项以json数据格式保存在codelabs_templates表config_json字段。一些配置项是通过一定规则自动计算的，一些配置项是需要可视化展示在界面供用户配置的，特别是与界面、展示相关的或指定数据源、接收用户输入/上传等一般都需用户配置。
4. 一般低代码/无代码框架，都是将控件配置结果以json结构保存到模板配置的json结构中，最后统一解析json生成结果代码。但是这种方案就需要开发者熟悉整体低代码/无代码产品的json规范才能比较合理的进行扩展开发。UCToo考虑到尽量降低自定义扩展开发的复杂度，因此采用了控件配置完成后，即生成了控件源码，最后合并进模板源码的方案。

## 安装教程
1. 可手动复制element模块前后端代码至本地开发环境项目目录，UCToo前后端模块代码目录。后台项目源码 gitee.com/uctoo/uctoo ,前端vue项目源码地址 https://gitee.com/UCT/uctoo-app-server-vue 
2. 管理员帐号登录管理后台安装element模块。
3. 由于代码生成属于开发阶段的工具，可能会对运营环境产生不可预知的影响，因此不建议在运营环境开放代码生成功能提供用户使用。demo.uctoo.com 测试环境仅提供体验界面，无执行权限。

### 运行环境依赖
1. 使用页面生成功能需先安装 composer require jaeger/querylist 和 composer require mck89/peast，querylist用于解析html标签，peast用于解析javascript代码。
2. 建议PHP版本大于7.4
3. 生成的CRUD页面源码依赖formOperate.js对应的版本，可从https://gitee.com/UCT/uctoo-app-server-vue 项目升级对应文件。

## 使用手册
    
### 模版

### 数据结构
1. 以input组件可配置项数据结构为例，在codelabs_templates表config_json字段使用了如下的数据结构。
```json

[{
  "interface": "input",
  "type": "string",
  "key": "v_model",
  "value": "",
  "uiSchema": {
    "type": "string",
    "label": "v_model",
    "placeholder": "绑定的数据变量",
    "display": "none",
    "x-component": "input",
    "x-decorator": "el-table-column"
  }
},
{
  "interface": "input",
  "type": "string",
  "key": "placeholder",
  "value": "",
  "uiSchema": {
    "type": "string",
    "label": "placeholder",
    "placeholder": "占位符",
    "display": "block",
    "x-component": "input",
    "x-decorator": "el-table-column"
  }
},
{
  "interface": "input",
  "type": "string",
  "key": "type",
  "value": "text",
  "uiSchema": {
    "type": "string",
    "label": "type",
    "placeholder": "text，textarea 和其他 原生 input 的 type 值",
    "display": "block",
    "x-component": "input",
    "x-decorator": "el-table-column"
  }
}]
```
### 算法

### 模板配置

### 代码生成

## 开发说明  

            
### 模块roadmap
1. 实现更多可自动生成的控件类型，使生成的页面不仅只管理员可用，还可以提供用户使用。
2. 在uctoo.com（或uctoo.org）提供codelabs_templates数据的开放接口和共享机制，类似packagist.org的作用。

## 扩展开发  
可以参考生成input和select控件的示例，进行更多可复用控件的扩展开发。建议的开发流程：  
1. 先根据需求或根据设计稿，写一个完整可用的页面控件。可以在test模块curd.vue页面中测试控件完整可用。 
2. 分析出页面控件可复用的部分以及需要动态替换的内容，抽取出公共部分形成可复用控件模板(参考select.stub模板)，控件文件结构需符合[vue单文件组件规范](https://cn.vuejs.org/v2/guide/single-file-components.html)。  
3. 根据可复用控件模板文件，写一个动态生成控件的方法（参考ElementComponents.php中的select方法），方法名请与控件名保持一致。
4. 在codelabs_templates表添加参照数据结构部分的json示例，添加控件可视化配置json数据。
5. 在element_components表添加上游技术栈的控件全量配置数据，虽然暂未用到但还是要添加。因为不同前端框架对同一类型控件可能标签是不同的，所以拆分成两个表。
6. 如需扩展uniapp的可视化配置控件库，就参考element模块新建一个uniapp控件模版库模块。
7. 在CreateElmformCommand命令行中，添加解析控件类型的代码。后续codelabs版本将废弃命令行方式，改由动态调用模板渲染接口方法的方案。

  具体请参考开源版开发手册 https://www.kancloud.cn/doc_uctoo/uctoo_dev 及 本开源项目示例  