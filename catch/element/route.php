<?php
// +----------------------------------------------------------------------
// | CatchAdmin [Just Like ～ ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017~{$year} http://catchadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( https://github.com/yanwenwu/catch-admin/blob/master/LICENSE.txt )
// +----------------------------------------------------------------------
// | Author: JaguarJack [ njphper@gmail.com ]
// +----------------------------------------------------------------------
// you should use `$router`
/* @var think\Route $router */
$router->group(function () use($router) {
    // elementComponents 路由
     $router->resource('elementComponents', catchAdmin\element\controller\ElementComponents::class);
    // 初始化控件配置
    $router->post('element/components/initconfig', '\catchAdmin\element\controller\ElementComponents@initconfig');
})->middleware('auth');
// input组件渲染
$router->post('element/components/input', '\catchAdmin\element\controller\ElementComponents@input');
// select组件渲染
$router->post('element/components/select', '\catchAdmin\element\controller\ElementComponents@select');
// upload组件渲染
$router->post('element/components/upload', '\catchAdmin\element\controller\ElementComponents@upload');
// datepicker组件渲染
$router->post('element/components/datepicker', '\catchAdmin\element\controller\ElementComponents@datepicker');
// switch组件渲染
$router->post('element/components/switch', '\catchAdmin\element\controller\ElementComponents@switch');
// chinaareadata组件渲染
$router->post('element/components/chinaareadata', '\catchAdmin\element\controller\ElementComponents@chinaareadata');