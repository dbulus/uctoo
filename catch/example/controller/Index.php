<?php

namespace catchAdmin\example\controller;

use catcher\base\CatchRequest as Request;
use catcher\CatchResponse;
use catcher\base\CatchController;
use catchAdmin\example\model\Example as exampleModel;

class Index extends CatchController
{
    protected $exampleModel;
    
    public function __construct(exampleModel $exampleModel)
    {
        $this->exampleModel = $exampleModel;
    }
    
    /**
     * 列表
     * @time 2021年02月02日 15:30
     * @param Request $request 
     */
    public function index(Request $request) : \think\Response
    {
        return CatchResponse::paginate($this->exampleModel->getList());
    }
    
    /**
     * 保存信息
     * @time 2021年02月02日 15:30
     * @param Request $request 
     */
    public function save(Request $request) : \think\Response
    {
        return CatchResponse::success($this->exampleModel->storeBy($request->post()));
    }
    
    /**
     * 读取
     * @time 2021年02月02日 15:30
     * @param $id 
     */
    public function read($id) : \think\Response
    {
        return CatchResponse::success($this->exampleModel->findBy($id));
    }
    
    /**
     * 更新
     * @time 2021年02月02日 15:30
     * @param Request $request 
     * @param $id
     */
    public function update(Request $request, $id) : \think\Response
    {
        return CatchResponse::success($this->exampleModel->updateBy($id, $request->post()));
    }
    
    /**
     * 删除
     * @time 2021年02月02日 15:30
     * @param $id
     */
    public function delete($id) : \think\Response
    {
        return CatchResponse::success($this->exampleModel->deleteBy($id));
    }
}