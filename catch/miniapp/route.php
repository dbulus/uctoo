<?php
// +----------------------------------------------------------------------
// | CatchAdmin [Just Like ～ ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017~{$year} http://catchadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( https://github.com/yanwenwu/catch-admin/blob/master/LICENSE.txt )
// +----------------------------------------------------------------------
// | Author: JaguarJack [ njphper@gmail.com ]
// +----------------------------------------------------------------------

// you should use `$router`
$router->group(function () use ($router){
	// wechatCloud路由
	$router->resource('wechatCloud', '\catchAdmin\miniapp\controller\WechatCloud');
    $router->get('miniapp/getCloud', '\catchAdmin\miniapp\controller\WechatCloud@getCloud');
    $router->get('miniapp/createMiniappCloud', '\catchAdmin\miniapp\controller\WechatCloud@createMiniappCloud');
	// wechatMiniappVersion路由
	$router->resource('wechatMiniappVersion', '\catchAdmin\miniapp\controller\WechatMiniappVersion');
    $router->get('miniapp/templateConfig/<id>', '\catchAdmin\miniapp\controller\WechatMiniappVersion@templateConfig');
    $router->get('miniapp/templateCommit/<id>', '\catchAdmin\miniapp\controller\WechatMiniappVersion@templateCommit');
    $router->get('miniapp/templatePreview/<id>', '\catchAdmin\miniapp\controller\WechatMiniappVersion@templatePreview');
    $router->get('miniapp/templateAudit/<id>', '\catchAdmin\miniapp\controller\WechatMiniappVersion@templateAudit');
    $router->get('miniapp/templateRelease/<id>', '\catchAdmin\miniapp\controller\WechatMiniappVersion@templateRelease');
    // 批量云开发静态网站路由
    $router->post('miniapp/createstaticstore', '\catchAdmin\miniapp\controller\WechatStaticStore@createstaticstore');
    $router->post('miniapp/describestaticstore', '\catchAdmin\miniapp\controller\WechatStaticStore@describestaticstore');
    $router->post('miniapp/staticfilelist', '\catchAdmin\miniapp\controller\WechatStaticStore@staticfilelist');
    $router->post('miniapp/staticuploadfile', '\catchAdmin\miniapp\controller\WechatStaticStore@staticuploadfile');
    // 批量云开发路由
    $router->post('batchcloud/createenv', '\catchAdmin\miniapp\controller\BatchCloud@createenv');
    $router->post('batchcloud/modifyenv/<env>', '\catchAdmin\miniapp\controller\BatchCloud@modifyenv');
    $router->post('batchcloud/describeenvs', '\catchAdmin\miniapp\controller\BatchCloud@describeenvs');
    $router->post('batchcloud/batchshareenv', '\catchAdmin\miniapp\controller\BatchCloud@batchshareenv');
    $router->post('batchcloud/batchgetenvid', '\catchAdmin\miniapp\controller\BatchCloud@batchgetenvid');
    // wxaUpdatableMessage 路由
     $router->resource('wxaUpdatableMessage', catchAdmin\miniapp\controller\WxaUpdatableMessage::class);
})->middleware('auth');