<?php

namespace catchAdmin\permissions\controller;

use catcher\base\CatchRequest as Request;
use catcher\CatchResponse;
use catcher\base\CatchController;
use catchAdmin\permissions\model\UserHasGroup as UserHasGroupModel;
use think\Response;

class UserHasGroup extends CatchController
{
    
    protected $userHasGroupModel;
    
    /**
     *
     * @time 2022/02/22 12:16
     * @param UserHasGroupModel $userHasGroupModel
     * @return mixed
     */
    public function __construct(UserHasGroupModel $userHasGroupModel)
    {
        $this->userHasGroupModel = $userHasGroupModel;
    }
    
    /**
     *
     * @time 2022/02/22 12:16
     * @return Response
     */
    public function index() : Response
    {
        return CatchResponse::paginate($this->userHasGroupModel->getList());
    }
    
    /**
     *
     * @time 2022/02/22 12:16
     * @param Request $request
     * @return Response
     */
    public function save(Request $request) : Response
    {
        return CatchResponse::success($this->userHasGroupModel->storeBy($request->post()));
    }
    
    /**
     *
     * @time 2022/02/22 12:16
     * @param $id
     * @return Response
     */
    public function read($id) : Response
    {
        return CatchResponse::success($this->userHasGroupModel->findBy($id));
    }
    
    /**
     *
     * @time 2022/02/22 12:16
     * @param $id
     * @param Request $request
     * @return Response
     */
    public function update($id, Request $request) : Response
    {
        return CatchResponse::success($this->userHasGroupModel->updateBy($id, $request->post()));
    }
    
    /**
     *
     * @time 2022/02/22 12:16
     * @param $id
     * @return Response
     */
    public function delete($id) : Response
    {
        return CatchResponse::success($this->userHasGroupModel->deleteBy($id));
    }
}