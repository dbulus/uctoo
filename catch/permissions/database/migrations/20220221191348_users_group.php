<?php

use think\migration\Migrator;
use think\migration\db\Column;
use Phinx\Db\Adapter\MysqlAdapter;

class UsersGroup extends Migrator
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('users_group', ['engine' => 'InnoDB', 'collation' => 'utf8mb4_general_ci', 'comment' => '通用用户组' ,'id' => 'id' ,'primary_key' => ['id']]);
        $table->addColumn('group_name', 'string', ['limit' => 255,'null' => false,'default' => '','signed' => true,'comment' => '用户组名称',])
			->addColumn('parent_id', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => true,'comment' => '父级ID',])
			->addColumn('code', 'string', ['limit' => 255,'null' => false,'default' => '','signed' => true,'comment' => '用户组唯一标识，用英文表示',])
			->addColumn('intro', 'string', ['limit' => 255,'null' => true,'signed' => true,'comment' => '用户组介绍',])
			->addColumn('created_at', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => false,'comment' => '创建时间',])
			->addColumn('updated_at', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => false,'comment' => '更新时间',])
			->addColumn('deleted_at', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => false,'comment' => '软删除字段',])
			->addColumn('creator_id', 'integer', ['limit' => MysqlAdapter::INT_REGULAR,'null' => false,'default' => 0,'signed' => false,'comment' => '创建人ID',])
			->addIndex(['code'], ['unique' => true,'name' => 'users_group_code'])
			->addIndex(['parent_id'], ['name' => 'users_group_parent_id'])
            ->create();
    }
}
