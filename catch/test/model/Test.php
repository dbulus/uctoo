<?php

namespace catchAdmin\test\model;

use catcher\base\CatchModel as Model;

class Test extends Model
{
    // 表名
    public $name = 'test';
    // 数据库字段映射
    public $field = array(
        'id',
        'module',
        'operate',
        'route',
        'params',
        'ip',
        // 创建人ID
        'creator_id',
        // 创建时间
        'created_at',
        // 更新时间
        'updated_at',
        // 软删除
        'deleted_at',
    );
}