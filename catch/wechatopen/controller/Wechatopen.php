<?php

namespace catchAdmin\wechatopen\controller;

use catcher\base\CatchRequest as Request;
use catcher\CatchResponse;
use catcher\base\CatchController;
use catchAdmin\wechatopen\model\Wechatopen as wechatopenModel;

class Wechatopen extends CatchController
{
    protected $wechatopenModel;
    
    public function __construct(WechatopenModel $wechatopenModel)
    {
        $this->wechatopenModel = $wechatopenModel;
    }
    
    /**
     * 列表
     * @time 2021年03月03日 18:58
     * @param Request $request 
     */
    public function index(Request $request) : \think\Response
    {
        return CatchResponse::paginate($this->wechatopenModel->getList());
    }
    
    /**
     * 保存信息
     * @time 2021年03月03日 18:58
     * @param Request $request 
     */
    public function save(Request $request) : \think\Response
    {
        return CatchResponse::success($this->wechatopenModel->storeBy($request->post()));
    }
    
    /**
     * 读取
     * @time 2021年03月03日 18:58
     * @param $id 
     */
    public function read($id) : \think\Response
    {
        return CatchResponse::success($this->wechatopenModel->findBy($id));
    }
    
    /**
     * 更新
     * @time 2021年03月03日 18:58
     * @param Request $request 
     * @param $id
     */
    public function update(Request $request, $id) : \think\Response
    {
        return CatchResponse::success($this->wechatopenModel->updateBy($id, $request->post()));
    }
    
    /**
     * 删除
     * @time 2021年03月03日 18:58
     * @param $id
     */
    public function delete($id) : \think\Response
    {
        return CatchResponse::success($this->wechatopenModel->deleteBy($id));
    }
}