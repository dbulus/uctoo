<?php
// +----------------------------------------------------------------------
// | CatchAdmin [Just Like ～ ]
// +----------------------------------------------------------------------
// | Copyright (c) 2017~2021 http://catchadmin.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( https://github.com/yanwenwu/catch-admin/blob/master/LICENSE.txt )
// +----------------------------------------------------------------------
// | Author: JaguarJack [ njphper@gmail.com ]
// +----------------------------------------------------------------------

// wechatopen路由// wechatopen路由;
$router->group(function () use ($router){
    $router->resource('wechatopen', '\catchAdmin\wechatopen\controller\Wechatopen');
	// applet路由
	$router->resource('applet', '\catchAdmin\wechatopen\controller\Applet');
    // 切换应用
    $router->get('applet/setapplet/<id>', '\catchAdmin\wechatopen\controller\Applet@setApplet');
    // 获取当前选中应用
    $router->get('admin/applet/<creator_id>', '\catchAdmin\wechatopen\controller\AdminApplet@adminApplet');
	// adminApplet路由
	$router->resource('admin/applet', '\catchAdmin\wechatopen\controller\AdminApplet');
    // 扫码授权回跳页
    $router->get('wechatauth', '\catchAdmin\wechatopen\controller\Wechatauth');
	// wechatopenUsers路由
	$router->resource('wechatopenUsers', '\catchAdmin\wechatopen\controller\WechatopenUsers');
    //微信扫码后获取后台帐号
    $router->get('wechatlogin/wechatoauth', '\catchAdmin\wechatopen\controller\WechatLogin@wechatoauth');
    //微信扫码后登录后台帐号
    $router->post('wechatlogin/wechatlogin', '\catchAdmin\wechatopen\controller\WechatLogin@wechatlogin');
    //微信扫码后注册后台帐号
    $router->post('wechatlogin/wechatregist', '\catchAdmin\wechatopen\controller\WechatLogin@wechatregist');
	// wechatopenMiniappUsers路由
	$router->resource('wechatopenMiniappUsers', '\catchAdmin\wechatopen\controller\WechatopenMiniappUsers');
    // 检测小程序用户自定义登录态token
    $router->post('api/miniapplogin/checktoken', '\catchAdmin\wechatopen\controller\MiniappLogin@checktoken');
    // 小程序登录
    $router->post('api/miniapplogin/wxlogin', '\catchAdmin\wechatopen\controller\MiniappLogin@wxlogin');
    // 小程序授权获取用户信息
    $router->post('api/miniapplogin/decryptInfo', '\catchAdmin\wechatopen\controller\MiniappLogin@decryptInfo');
    // 小程序授权获取用户电话
    $router->post('api/miniapplogin/decryptPhone', '\catchAdmin\wechatopen\controller\MiniappLogin@decryptPhone');
    // 小程序端获取后台管理员帐号
    $router->post('api/wechatopen/users', '\catchAdmin\wechatopen\controller\User@read')->middleware(\uctoo\middleware\AuthWechatTokenMiddleware::class);
    // 小程序用户组
    $router->post('wechatopenMiniappUser/addGroups', '\catchAdmin\wechatopen\controller\WechatopenMiniappUsers@addGroups')->middleware(\uctoo\middleware\AuthWechatTokenMiddleware::class);
    $router->post('wechatopenMiniappUser/getGroups', '\catchAdmin\wechatopen\controller\WechatopenMiniappUsers@getGroups')->middleware(\uctoo\middleware\AuthWechatTokenMiddleware::class);
    $router->post('wechatopenMiniappUser/delGroups', '\catchAdmin\wechatopen\controller\WechatopenMiniappUsers@delGroups')->middleware(\uctoo\middleware\AuthWechatTokenMiddleware::class);
    // 微信第三方平台用户组
    $router->post('wechatopenUser/addGroups', '\catchAdmin\wechatopen\controller\WechatopenUsers@addGroups')->middleware(\uctoo\middleware\AuthWechatTokenMiddleware::class);
    $router->post('wechatopenUser/getGroups', '\catchAdmin\wechatopen\controller\WechatopenUsers@getGroups')->middleware(\uctoo\middleware\AuthWechatTokenMiddleware::class);
    $router->post('wechatopenUser/delGroups', '\catchAdmin\wechatopen\controller\WechatopenUsers@delGroups')->middleware(\uctoo\middleware\AuthWechatTokenMiddleware::class);
});