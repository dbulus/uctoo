<?php
declare (strict_types = 1);

namespace uctoo\middleware;

//use app\model\saas\MerchantAccount;
use catcher\CatchResponse;
use catcher\exceptions\PermissionForbiddenException;
use think\facade\Log;
use uctoo\util\exception\TokenException;
use think\exception\HttpResponseException;
use think\Response;
use catchAdmin\wechatopen\service\TokenService;

class AuthWechatTokenMiddleware
{
    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure       $next
     * @return Response
     */
    public function handle($request, \Closure $next)
    {
        $token = $request->header('token');  //todo 没用JWT，先明文传输了
        $check = (new TokenService())->check($token);

        if($check){
            //从后台缓存获取微信用户信息
            $user= cache($token);
            $request->user = $user;
        }else{
            throw new PermissionForbiddenException();
        }
        return $next($request);
    }
}
