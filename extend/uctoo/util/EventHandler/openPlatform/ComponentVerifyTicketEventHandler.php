<?php
// +----------------------------------------------------------------------
// | UCToo [ Universal Convergence Technology ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014-2021 https://www.uctoo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Patrick <contact@uctoo.com>
// +----------------------------------------------------------------------

namespace uctoo\util\EventHandler\openPlatform;

use catchAdmin\wechatopen\model\Applet;
use EasyWeChat\Kernel\Contracts\EventHandlerInterface;
use EasyWeChat\OpenPlatform\Application;
use think\facade\Cache;
use uctoo\ThinkEasyWeChat\Facade;
use catchAdmin\wechatopen\model\Wechatopen;

class ComponentVerifyTicketEventHandler implements EventHandlerInterface
{
    /**
     * @var Application
     */
    protected $app;
    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function handle($payload = null)
    {
        Wechatopen::where('appid', $payload['AppId'])->update(['component_verify_ticket' => $payload['ComponentVerifyTicket']]);
    }

}