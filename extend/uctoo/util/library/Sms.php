<?php
// +----------------------------------------------------------------------
// | UCToo [ Universal Convergence Technology ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014-2021 https://www.uctoo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Patrick <contact@uctoo.com>
// +----------------------------------------------------------------------

namespace uctoo\util\library;

use app\model\Sms as SmsModel;

/**
 * 短信验证码类
 */
class Sms
{

    /**
     * 验证码有效时长
     * @var int
     */
    protected static $expire = 300;

    /**
     * 最大允许检测的次数
     * @var int
     */
    protected static $maxCheckNums = 10;

    /**
     * 获取最后一次手机发送的数据
     *
     * @param int $mobile 手机号
     * @param string $event 事件
     * @return array|\think\Model|null
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function get($mobile, $event = 'default')
    {
        $sms = SmsModel::where(['mobile' => $mobile, 'event' => $event])
            ->order('id', 'DESC')
            ->find();
        return $sms ? $sms : null;
    }

    /**
     * 发送验证码
     */
    public static function send($mobile, $code = null, $event = 'default')
    {

    }

    /**
     * 发送通知
     */
    public static function notice($mobile, $msg = '', $template = null)
    {

    }

    /**
     * 校验验证码
     *
     * @param int $mobile 手机号
     * @param int $code 验证码
     * @param string $event 事件
     * @return  boolean
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\DbException
     * @throws \think\db\exception\ModelNotFoundException
     */
    public static function check($mobile, $code, $event = 'default')
    {
        $time = time() - self::$expire;
        $sms = SmsModel::where(['mobile' => $mobile, 'event' => $event])
            ->order('id', 'desc')
            ->find();
        if ($sms) {
            if ($sms->getData('createtime') > $time && $sms['times'] <= self::$maxCheckNums) {
                if($code == $sms['code']) {
                    return true;
                }else{
                    $sms->times = $sms->times + 1;
                    $sms->save();
                    return false;
                }
            } else {
                // 过期则清空该手机验证码
                self::flush($mobile, $event);
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * 清空指定手机号验证码
     *
     * @param int $mobile 手机号
     * @param string $event 事件
     * @return  boolean
     * @throws \Exception
     */
    public static function flush($mobile, $event = 'default')
    {
        SmsModel::where(['mobile' => $mobile, 'event' => $event])
            ->delete();
        return true;
    }
}
