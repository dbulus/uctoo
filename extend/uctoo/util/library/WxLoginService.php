<?php
// +----------------------------------------------------------------------
// | UCToo [ Universal Convergence Technology ]
// +----------------------------------------------------------------------
// | Copyright (c) 2014-2021 https://www.uctoo.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Patrick <contact@uctoo.com>
// +----------------------------------------------------------------------

namespace uctoo\util\library;


use EasyWeChat\OfficialAccount\Application;
use think\facade\Config;

class WxLoginService
{
    public static function instance($options = []){
        $config = Config::get('wxlogin');
        $app = new Application(array_merge($config,$options));//TODO:从管理后台配置获取
        $app->rebind('cache',app('cache'));
        return $app;
    }
}