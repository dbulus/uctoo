<img src="https://gitlab.com/mschop/notee/raw/master/logo.png" alt="logo" />

## Links
[DOCUMENTATION](https://notee.readthedocs.io)

The better alternative to template engines.

    <?php
    
    return _document(
        _html(
            _head(
                _title('Build HTML the awesome way'),
            ),
            _body(
                _h1('Why should you choose NoTee'),
                _ul(
                    map($reasonsForNoTee, fn($reason) => _li(['class' => 'reason'], $reason))
                ),
            )
        )
    )

# What is NoTee

NoTee is a library especially built for generating HTML5. It is not a general purpose template engine.

Compared to traditional template engines like Twig or Smarty, NoTee has the following advantages:

- more secure
- less error-prone
- simple/easy setup (no compile step)
- debuggable
- testable
- immutable node tree (unlimited node reuse)
- event system
- less verbose html
- functional programming

## More Secure

HTML is text. Without parsing the html you would not have any semantics of the contained data.

For more detail see section `security` in documentation.

## Less Error-Prone

By using NoTee, you can never have enclosing tag or quote bugs.

## Simple / Easy Setup

Using NoTee just needs a few lines of code in your application. You don't need any writable directory (for compiled
templates). NoTee is fast without caches.

## Debuggable

NoTee makes use of normal PHP. Just use xdebug (or any other debugger) to step through your view layer.

## Testable

Testing a view layer, that is based on NoTee is much simpler, because the html is in a uniformed style.

## Immutable Node Tree

The node factory produces an immutable node tree. Because of that, you can reuse nodes, without running into problems.

## Event System

NoTee has a powerful event system. You need to add a csrf token to every form-element? No problem. Just register
an event listener and create one upon form node creation.

## Less Verbose HTML

NoTee produces the least verbose output possible. E.g. in HTML5 it is possible to omit the closing </li> tag. This
reduces the response size. NoTee tries to produce the smallest html possible. White-Spaces are not there by default.

## Functional Programming

Because you are using pure PHP, you can use functional programming. NoTee works well with functional concepts.