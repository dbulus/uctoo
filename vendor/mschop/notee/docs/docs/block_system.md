The block system is enabled by default. The block system is build around two functions. The first
one is the function `_block(...) / $nf->block(...)`. With this method, you can define a block this way:

    <?php
    
    _body(
        _div(
            ['id' => 'site-wrapper'],
            _block('site-wrapper', fn() => _div('Hello World'))
        )
    )
    
The other function is `_extend(...) / $nf->extend(...)`. With this function you can override or extend an existing block.

```
<?php

_extend('site-wrapper', fn($parent) => _wrapper(
    $parent(),
    _div('Hello People')
));
```

Both examples combined would produce the following output:

```
<body>
    <div id="site-wrapper">
        <div>Hello World</div>
        <div>Hello People</div>
    </div>
</body>
```