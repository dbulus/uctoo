You can register an event subscriber to NoTee. This subscriber can be a filter. With an event subscriber you can react
to node creation. The mechanism is very flexible. The following example demonstrates the functionality with a listener,
that adds in hidden csrf token to every insecure form.

```
<?php

require 'vendor/autoload.php';

namespace YourNamespace;

use NoTee\NodeFactory;
use NoTee\DefaultEscaper;
use NoTee\UriValidator;
use NoTee\BlockManager;
use NoTee\SubscriberInterface;

$nf = new NodeFactory(
    new DefaultEscaper('utf-8'),
    new UriValidator(),
    new BlockManager()
);

$nf->subscribe(new class() implements SubscriberInterface {
    public function notify(NodeFactory $nodeFactory, DefaultNode $node): DefaultNode;
    {
        if ($node->getTagName() === 'form' && $this->isMethodSecure($node->getAttributes()['method'] ?? 'GET')) {
            return new DefaultNode(
                $node->getTagName(),
                $nodeFactory->getEscaper(),
                $node->getAttributes(),
                array_merge(
                    $node->getChildren(),
                    [$nodeFactory->input(['type' => 'hidden', 'name' => 'csrf_token', 'value' => 'mytoken'])]
                )
            );
        } else {
            return $node;
        }
    }

    private function isMethodSecure(string $method)
    {
        return in_array($method, ['GET', 'HEAD']); // pseudo implementation. do not use this in production.
    }
});
```