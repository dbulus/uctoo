<?php

use NoTee\AbstractNodeFactory;
use NoTee\NodeFactory;
use NoTee\NodeInterface;
use NoTee\Nodes\BlockNode;
use NoTee\Nodes\DefaultNode;
use NoTee\Nodes\DocumentNode;
use NoTee\Nodes\RawNode;
use NoTee\Nodes\TextNode;
use NoTee\Nodes\WrapperNode;

global $noTee;

if (!$noTee instanceof NodeFactory) {
    throw new \Exception('You need to add a global noteephp nodefactory: global $noTee = new NodeFactory(...);');
}

function _node(string $name, array $attributes = [], array $children = []): DefaultNode
{
    global $noTee;
    assert($noTee instanceof NodeFactory);
    return new DefaultNode(
        $name,
        $noTee->getEscaper(),
        $attributes,
        $children
    );
}

function _raw(string $text): RawNode
{
    global $noTee;
    assert($noTee instanceof NodeFactory);
    return $noTee->raw($text);
}

function _text(string $text): TextNode
{
    global $noTee;
    assert($noTee instanceof NodeFactory);
    return $noTee->text($text);
}

function _wrapper(...$args): WrapperNode
{
    global $noTee;
    assert($noTee instanceof NodeFactory);
    return call_user_func_array([$noTee, 'wrapper'], $args);
}

function _document(NodeInterface $topNode): DocumentNode
{
    return new DocumentNode($topNode);
}

function _block(string $name, callable $callable): BlockNode
{
    global $noTee;
    assert($noTee instanceof NodeFactory);
    return $noTee->block($name, $callable);
}

function _extend(string $name, callable $callable): void
{
    global $noTee;
    assert($noTee instanceof NodeFactory);
    $noTee->extend($name, $callable);
}

function _include(string $file): NodeInterface
{
    global $noTee;
    assert($noTee instanceof NodeFactory);
    return $noTee->include($file);
}

function _a(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('a', $args);
}

function _abbr(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('abbr', $args);
}

function _acronym(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('acronym', $args);
}

function _address(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('address', $args);
}

function _applet(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('applet', $args);
}

function _area(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('area', $args);
}

function _article(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('article', $args);
}

function _aside(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('aside', $args);
}

function _audio(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('audio', $args);
}

function _base(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('base', $args);
}

function _basefont(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('basefont', $args);
}

function _b(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('b', $args);
}

function _bdo(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('bdo', $args);
}

function _bgsound(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('bgsound', $args);
}

function _big(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('big', $args);
}

function _blink(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('blink', $args);
}

function _blockquote(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('blockquote', $args);
}

function _body(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('body', $args);
}

function _br(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('br', $args);
}

function _button(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('button', $args);
}

function _canvas(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('canvas', $args);
}

function _caption(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('caption', $args);
}

function _center(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('center', $args);
}

function _cite(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('cite', $args);
}

function _code(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('code', $args);
}

function _col(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('col', $args);
}

function _colgroup(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('colgroup', $args);
}

function _command(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('command', $args);
}

function _datalist(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('datalist', $args);
}

function _dd(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('dd', $args);
}

function _del(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('del', $args);
}

function _details(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('details', $args);
}

function _dfn(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('dfn', $args);
}

function _div(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('div', $args);
}

function _dl(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('dl', $args);
}

function _dt(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('dt', $args);
}

function _embed(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('embed', $args);
}

function _em(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('em', $args);
}

function _fieldset(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('fieldset', $args);
}

function _figcaption(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('figcaption', $args);
}

function _figure(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('figure', $args);
}

function _font(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('font', $args);
}

function _footer(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('footer', $args);
}

function _form(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('form', $args);
}

function _frame(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('frame', $args);
}

function _frameset(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('frameset', $args);
}

function _h1(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('h1', $args);
}

function _h2(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('h2', $args);
}

function _h3(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('h3', $args);
}

function _h4(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('h4', $args);
}

function _h5(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('h5', $args);
}

function _h6(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('h6', $args);
}

function _header(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('header', $args);
}

function _head(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('head', $args);
}

function _hgroup(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('hgroup', $args);
}

function _hr(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('hr', $args);
}

function _html(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('html', $args);
}

function _iframe(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('iframe', $args);
}

function _i(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('i', $args);
}

function _img(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('img', $args);
}

function _input(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('input', $args);
}

function _ins(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('ins', $args);
}

function _isindex(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('isindex', $args);
}

function _kbd(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('kbd', $args);
}

function _keygen(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('keygen', $args);
}

function _label(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('label', $args);
}

function _legend(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('legend', $args);
}

function _li(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('li', $args);
}

function _link(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('link', $args);
}

function _listing(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('listing', $args);
}

function _map(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('map', $args);
}

function _mark(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('mark', $args);
}

function _marquee(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('marquee', $args);
}

function _math(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('math', $args);
}

function _menu(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('menu', $args);
}

function _meta(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('meta', $args);
}

function _meter(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('meter', $args);
}

function _nav(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('nav', $args);
}

function _nextid(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('nextid', $args);
}

function _nobr(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('nobr', $args);
}

function _noembed(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('noembed', $args);
}

function _noframes(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('noframes', $args);
}

function _noscript(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('noscript', $args);
}

function _object(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('object', $args);
}

function _ol(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('ol', $args);
}

function _optgroup(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('optgroup', $args);
}

function _option(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('option', $args);
}

function _output(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('output', $args);
}

function _param(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('param', $args);
}

function _plaintext(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('plaintext', $args);
}

function _p(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('p', $args);
}

function _pre(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('pre', $args);
}

function _progress(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('progress', $args);
}

function _q(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('q', $args);
}

function _rp(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('rp', $args);
}

function _rt(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('rt', $args);
}

function _ruby(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('ruby', $args);
}

function _samp(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('samp', $args);
}

function _script(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('script', $args);
}

function _section(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('section', $args);
}

function _select(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('select', $args);
}

function _small(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('small', $args);
}

function _source(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('source', $args);
}

function _spacer(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('spacer', $args);
}

function _span(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('span', $args);
}

function _s(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('s', $args);
}

function _strike(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('strike', $args);
}

function _strong(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('strong', $args);
}

function _style(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('style', $args);
}

function _sub(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('sub', $args);
}

function _sup(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('sup', $args);
}

function _summary(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('summary', $args);
}

function _svg(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('svg', $args);
}

function _table(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('table', $args);
}

function _tbody(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('tbody', $args);
}

function _td(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('td', $args);
}

function _textarea(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('textarea', $args);
}

function _tfoot(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('tfoot', $args);
}

function _thead(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('thead', $args);
}

function _th(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('th', $args);
}

function _time(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('time', $args);
}

function _title(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('title', $args);
}

function _tr(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('tr', $args);
}

function _tt(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('tt', $args);
}

function _ul(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('ul', $args);
}

function _u(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('u', $args);
}

function _var(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('var', $args);
}

function _video(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('video', $args);
}

function _wbr(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('wbr', $args);
}

function _xmp(...$args): DefaultNode
{
    global $noTee;
    assert($noTee instanceof AbstractNodeFactory);
    return $noTee->create('xmp', $args);
}
