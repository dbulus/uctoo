<?php

declare(strict_types=1);

namespace NoTee\Nodes;


use NoTee\EscaperInterface;
use NoTee\NodeInterface;

class DefaultNode implements NodeInterface
{
    // Void tags are html tags, that are not allowed to have child elements.
    public const VOID_TAGS = [
        'area',
        'base',
        'br',
        'col',
        'command',
        'embed',
        'hr',
        'img',
        'input',
        'keygen',
        'link',
        'meta',
        'param',
        'source',
        'track',
        'wbr',
    ];

    // those tags must not have an closing tag, if there are no succeeding elements
    public const TAGS_WITH_OPTIONAL_CLOSING = [
        'html',
        'head',
        'body',
        'p',
        'dt',
        'dd',
        'li',
        'option',
        'thead',
        'th',
        'tbody',
        'tr',
        'td',
        'tfoot',
        'colgroup',
    ];

    protected string $tagName;
    protected EscaperInterface $escaper;
    protected array $attributes;
    /** @var array Node[] */
    protected array $children;

    public function __construct(string $tagName, EscaperInterface $escaper, array $attributes = [], array $children = [])
    {
        $this->tagName = $tagName;
        $this->escaper = $escaper;
        $this->attributes = $attributes;
        foreach ($children as &$child) {
            if (!is_object($child)) {
                $child = new TextNode((string)$child, $this->escaper);
            }
        }
        $this->children = $children;
    }

    public function __toString(): string
    {
        $attributeString = !empty($this->attributes) ? ' ' . $this->getAttributeString() : '';

        if (in_array($this->tagName, static::VOID_TAGS)) {
            if (!empty($this->children)){
                throw new \Exception('Void tags cannot have children');
            }
            return "<{$this->tagName}{$attributeString}/>";
        }

        $children = implode('', $this->children);
        $result = "<{$this->tagName}$attributeString>$children";
        if (!in_array($this->tagName, static::TAGS_WITH_OPTIONAL_CLOSING)) {
            $result .= "</{$this->tagName}>";
        }
        return $result;
    }

    public function getAttributeString(): string
    {
        $attributeString = '';
        $first = true;
        foreach ($this->attributes as $name => $value) {
            $escapedAttribute = $this->escapeAttribute($value);
            $attributeString .= ($first ? '' : ' ') . $name . '="' . $escapedAttribute . '"';
            $first = false;
        }
        return $attributeString;
    }

    /**
     * @return string
     */
    public function getTagName(): string
    {
        return $this->tagName;
    }

    /**
     * @return EscaperInterface
     */
    public function getEscaper(): EscaperInterface
    {
        return $this->escaper;
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        return $this->attributes;
    }

    /**
     * @return NodeInterface[]
     */
    public function getChildren(): array
    {
        return $this->children;
    }

    protected function escapeAttribute(string $value): string
    {
        return $this->escaper->escapeAttribute($value);
    }
}
