<?php


namespace NoTee;


interface TemplateInterface
{
    public function render(string $template, array $context = []): NodeInterface;
}