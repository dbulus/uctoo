<?php

declare(strict_types=1);

namespace NoTee;

/**
 * Class UriValidator
 *
 * Ensure that attributes, containing URI only contain secure schemes (e.g. "javascript:" is not allowed for security)
 *
 * @package NoTee
 */
class UriValidator implements UriValidatorInterface
{
    protected const SCHEME_WHITELIST = [
        '',
        'http',
        'https',
        'ftp',
        'ftps',
        'sftp'
    ];

    public function isValid(string $uri) : bool
    {
        $uri = trim($uri);
        if (empty($uri)) return true;
        $parsed = parse_url($uri);
        if (!$parsed) return false; // if parsing fails, there is something wrong with this uri
        if (!isset($parsed['scheme'])) return true; // the parsing was successful, but no scheme was provided. This should be secure.
        $scheme = mb_strtolower($parsed['scheme']);
        return in_array($scheme, static::SCHEME_WHITELIST);
    }
}
