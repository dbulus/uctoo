<?php


namespace NoTee;


use PHPUnit\Framework\TestCase;
use function _a;

class GlobalTest extends TestCase
{
    public function test()
    {
        $this->assertEquals('<a></a>', (string)_a());
        global $noTee;
        $noTee = new NodeFactory(new DefaultEscaper('utf-8'), new UriValidator(), new BlockManager(), true);
        require_once __DIR__ . '/../global.php';
        $a = _a(); $line = __LINE__;
        $this->assertEquals(__FILE__ . ':' . $line, $a->getAttributes()['data-source']);
    }
}