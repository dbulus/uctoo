<?php

declare(strict_types=1);

namespace NoTee;

use PHPUnit\Framework\TestCase;

class NodeFactoryTest extends TestCase
{

    private NodeFactory $nf;
    private NodeFactory $debugNf;

    /**
     * @before
     */
    public function before()
    {
        $this->nf = new NodeFactory(new DefaultEscaper('utf-8'), new UriValidator(), new BlockManager());
        $this->debugNf = new NodeFactory(new DefaultEscaper('utf-8'), new UriValidator(), new BlockManager(), true);
    }

    public function Test_ComplexStructure()
    {

        $node = $this->nf->div(
            [],
            $this->nf->span(
                ['class' => 'class1 class2'],
                'this library is for writing ',
                $this->nf->abbr(['title' => 'Hypertext Markup Language'], 'html')
            )
        );

        $expected = '<div><span class="class1 class2">this library is for writing <abbr title="Hypertext Markup Language">html</abbr></span></div>';
        $this->assertEquals($expected, (string)$node);
    }

    public function test()
    {
        $node = $this->nf->div(
            $this->nf->a(
                'hello world'
            )
        );
        $this->assertEquals('<div><a>hello world</a></div>', (string)$node);


        $node = $this->nf->div(
            [$this->nf->a(), $this->nf->abbr()],
            $this->nf->span()
        );
        $this->assertEquals('<div><a></a><abbr></abbr><span></span></div>', (string)$node);

        $node = $this->nf->div(
            ['class' => 'hello'],
            [$this->nf->a(), $this->nf->abbr()],
            null,
            $this->nf->span('test')
        );
        $this->assertEquals('<div class="hello"><a></a><abbr></abbr><span>test</span></div>', $node->__toString());

    }

    public function test_debugMode()
    {
        $root = $this->debugNf->span(); $line = __LINE__;
        $file = __FILE__;
        $source = "$file:$line";
        $this->assertEquals('<span data-source="' . $source . '"></span>', (string)$root);
    }

    public function test_debugModeWithExistingAttributes()
    {
        $root = $this->debugNf->span(['id' => 'hello-world']); $line = __LINE__;
        $file = __FILE__;
        $source = "$file:$line";
        $this->assertEquals('<span id="hello-world" data-source="' . $source . '"></span>', (string)$root);
    }

    public function test_textAndRaw()
    {
        $node = $this->nf->text('test>');
        $this->assertEquals('test&gt;', (string)$node);
        $node = $this->nf->raw('test>');
        $this->assertEquals('test>', (string)$node);
    }

    public function testDocument()
    {
        $doc = $this->nf->document(
            $this->nf->html(
                $this->nf->head(

                ),
                $this->nf->body(
                    'test'
                )
            )
        );
        $this->assertEquals("<!DOCTYPE html><html><head><body>test", (string)$doc);
    }

    public function test_issue57_firstItemInFirstParameterIsNull_shouldBeInterpretedAsNode()
    {
        $node = $this->nf->div([null, $this->nf->text('test')]);
        $this->assertEquals('<div>test</div>', (string)$node);
    }

    public function test_childNeitherObjectNorString()
    {
        $node = $this->nf->div('1', 1);
        $this->assertEquals($this->nf->div('1', '1'), $node);
    }

    public function test_wrapper()
    {
        $wrapper = $this->nf->wrapper(
            $this->nf->div('test'),
            [
                $this->nf->div('test2'),
                'test3'
            ]
        );
        $this->assertEquals('<div>test</div><div>test2</div>test3', (string)$wrapper);
    }
}
